#VMD instalation

The package contained here (**vmd-1.9.4a31.tar.gz**) was obtained from the VMD website (https://www.ks.uiuc.edu/Research/vmd/). 

In the following it is assumed that the reader is using a Linux distribution of 64-bit.

Depending on the privileges of the user (if he/she has the SU password or not) the following command are necessary:

###Without SU privileges
**1.-** Create a folder named *vmd* in your home directory.

**2.-** Inside this folder open a terminal and type **pwd**. This will show the current location of the terminal. For example */home/vmd/*

**3.-** Untar the file *vmd-1.9.4a31.tar.gz*. This will crete a folder with the same name. Inside this folder you will find the file **configure**; open it with, for example, gedit. Then change lines 16 and 19 in the following way:

**For line 16:**
from: $install_bin_dir="/usr/local/bin";

to: $install_bin_dir="/home/vmd/bin";

**For line 19:**
from: $install_library_dir="/usr/local/lib/$install_name";

to: $install_library_dir="/home/vmd/lib/$install_name";

**4.-** Inside the folder where the file *configure* is located, open a terminal and type: **./configure**

**5.-** With the same terminal go to the folder *src/* (not the one in LAMMPS but the one inside vmd-1.9.4a31). Then type **make install**

**6.-** VMD should be now installed. The way to call it is by typing in a terminal **/home/vmd/bin/vmd**

###With SU privileges
**1.-** Untar the file *vmd-1.9.4a31.tar.gz*. This will crete a folder with the same name. Open a terminal inside this folder.

**2.-** type: **./configure**

**3.-** With the same terminal go to the folder *src/* (not the one in LAMMPS but the one inside vmd-1.9.4a31). Then type **sudo make install**. It will ask for the SU password, type it and press enter.

**4.-**  VMD should be now installed. The way to call it is by typing in a terminal **vmd**