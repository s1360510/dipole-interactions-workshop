##Metallic particles under the influence of a constant magnetic field

Here we try to model a system of *N* metallic particles confined to a cylindrical recipient. The particles lie on the xy plane and are under the influence of a constant magnetic field (**B**, pointing along the z direction). This field produces a magnetic moment (**mu**) on each particle. Due to this the particles interact with each other and we can have different scenarios:

    a) When the particles are only allowed to move on the xy plane and **mu** is aligned with **B**: The resulting interaction between each pair of particles is purely repulsive and scales as $1/r⁵$ 
    
    b) When the particles can move freely in the 3D and **mu** is aligned with **B**: There is a dipole-dipole interaction between each pair of particles that produces a force **Fpp** (please see the [Link](https://lammps.sandia.gov/doc/pair_dipole.html) for a description of **Fpp**) and a torque **Tpp** that changes the orientation of **mu** during the simulation.

For details in the system we refer the reader to:
C. Tapia-Ignacio, R. E. Moctezuma, and F. Donado. *"Structure and fragility in a macroscopic model of a glass-forming liquid based on a nonvibrating granular system"* PHYSICAL REVIEW E 98, 032901 (2018)

###Note:
This tutorial is thought for people without previous knowledge of LAMMPS. We tried to generate a practical example where the reader can:

    1.- Compile LAMMPS easily (see the readme files-inside*lammps_compilation*).
    2.- Install VMD (see inside the respective folder) to visualize the trajectory obtained from the simulations. 
    3.-Understand and run the LAMMPS scripts written for this particular system (see inside *scripts*)
    
However, we encourage the reader to constantly review the LAMMPS manual to get an idea of what is done here.