We modified different potentials (the ones that come with LAMMPS) to mimic the magnetic dipole interaction in the system. To install these new potentials and compile LAMMPS you just have to:

**Compilation of LAMMPS in serial**

    1.- Copy the *.cpp* and *.h* files into the *src/* directory (this is inside the folder *lammps-master*, the one that comes with the original LAMMPS distribution). 
    
    2.-Inside *src/* there is a folder called *STUBS*. Go to this location in a terminal and type **make**. This will compile some libraries.
    
    3.-In a terminal move again to the *src/* folder. Then you just have to enable some packages by typing:
             make yes-asphere
             make yes-dipole
             make yes-misc
    Please note that these packages have been chosen because the type of particles we are dealing with in the real system.
   
    4.- Finally type: make serial. Please note that some warnigs might appear, yous should not worry about them. This will create the executable called **lmp_serial**. This is used by typing the command *./lmp_serial -in lammps_script*, where *lammps_script* contains the instructions for the integration of motion (an example is shown inside *script/2_equilibration_with_Soft/*)
    
**Compilation of LAMMPS in parallel**

Follow the same instruction as in steps 1 to 3 from above. Then we have to change lines 9 and 14 from the file **Makefile.mpi** located inside **src/MAKE/**

    a) line9:
    from: CC =		mpicxx
    to: CC =		/usr/lib64/openmpi/bin/mpicxx
    
    b)line 14:
    from: LINK =		mpicxx
    to: LINK =		/usr/lib64/openmpi/bin/mpicxx
    
Finally, open a terminal inside **src/** and type **make mpi**. This will create the executable **lmp_mpi**. This is used by typing the command *./lmp_mpi -np 4 -in lammps_script*, where np is the number of processors to be used,

For some clusters (like the one in the UAEH) we will require the command: 

    /usr/lib64/openmpi/bin/mpirun -display-map -npernode 12 -hostfile node1 -np 12 ./lmp_mpi -in lammps_Script &
    

**Below there is an explanation of the modifications made to the potentials. We also state in which case these potentials can be used.**

#### Modification of the Coulomb potential: accounting only for repulsive interactions 
For the case in which the magnetic field is oriented along the z direction, the dipole vector of the particles tends to align with it. Therefore, there is only a repulsive force interaction (that scales as $1/r^4$ in the direction of the unitary vector r) between the particles. This can be easily implemented if we modify the **pair_style lj/cut/coul/cut** (where the coulomb interaction scales as $1/r^3$) and we create the new **pair_style lj/cut/mcoul/cut** where the force scales as $1/r^5$ and the potential energy as $1/r^3$. This potential can be used in 2D simulations with either point-dipole particles (atom style dipole) or with sphere-dipole particles (atom style hybrid sphere dipole). In both cases we will use the *fix langevin* and *fix enforce2D* commands. However, while for the point particles we can use simply *fix nve* to integrate the equations of motion, for the sphere particles is necessary to use the *fix nve/sphere update dipole*. The integration for the point particles is therefore more efficient than for the sphere particles. In both cases the dipole moment will remain pointing along the z direction.

#### Modification of the electric dipole potential: accounting for both repulsive interactions and a torque
The **pair_style lj/cut/dipole/cut** potential was modified to include only the LJ and dipole-dipole interactions. The new potential is called **pair_lj_cut_mdipole_cut**. In this potential the coulomb interaction (Eqq, Fqq) and the charge-dipole interaction (Eqp, Fqp, Tpq and Tqp) are set to zero. It is necessary to use the *atom style hybrid sphere dipole* and therefore the *fix nve/sphere update dipole* integrations. In addition, the *fix langevin* is necessary for the brownian motion and the fix *enforce2D* to ensure 2D simulations. If in the initial configuration the dipole-moment is oriented along the z direction and the movement of the particles is allowed only in the xy plane, we will end up with the same scenario as in the previous potential (the dipole-moment will always point along the z direction and there will be a repulsive interaction between the particles that scales as $1/r^5$).

#### Implementation of the external-constant-magnetic-field
The presence of a constant magnetic field will tend to cause a **torque**=**mu** x **B** on the dipoles, so the dipole-moment of the particles (**mu**) tends to align with the magnetic field (**B**). The potential energy of this case is given as U=**mu** dot **B**. We modified the **fix efield** so the force due to the magnetic field is B (a constant) and the torque is given as **mu** cross **B**. This new fix doesn't account for the charge-field interaction (Lorentz force). Therefore, the only effect is to produce a torque so the dipole-moments align to the field. Please note that the package MISC is necessary to use this fix. 

NOTE: fix bfield was modified to work with constant fields (bx, by, bz). We didn't touch the part of the code corresponding to the *variable field*. Therefore, for a variable field it will act as an electric field (F=qE) and not as a magnetic field.