#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>      
#include <unistd.h>
#include <stdio.h>     
#include <time.h>
#include <iomanip>
#include <ctime>
#include <random> //For the random position of proteins

using namespace std;

int main()
{
    int N;
    cout << "Number of particles in the system: ";
    cin >> N;

    //Distance in simulations units means 1 sigma = 1 mm.
    double d;
    cout << "Diameter of particles (in simulation units): ";
    cin >> d;
 
    //charge
    double q;
    cout << "Charge of particles (in simulation units): ";
    cin >> q;


    /*The box size in simulations units*/
    int L;
    cout << "Box size along the x-y directions (in simulation units): ";
    cin >> L; 


    /*Initial position of particles*/
    vector<vector <double> > position(3, vector<double>(N));  

 
    int i,j,k,l;

 
/*************************************/
/*Position of particles in the system*/
/*************************************/
//THE X RANDOM POSITION 
    //generates a random seed (rd)
    random_device rdx;
    //random number generator (rng)
    mt19937 rngx(rdx());
    //produces random DECIMALS in the range from -L/2 to L/2 (inclusive)
    uniform_real_distribution<double> distrx(-(L/4.0)+1.0, (L/4.0)-1.0);

    //produces random INTEGERS in the range from -L/2 to L/2 (inclusive)
    //uniform_int_distribution<int> distrx(-(L/2.0), (L/2.0));

//THE Y RANDOM POSITION OF THE BOTTOM PATCH OF A PROTEIN
    random_device rdy;
    mt19937 rngy(rdy());
    uniform_real_distribution<double> distry(-(L/4.0)+1.0, (L/4.0)-1.0);


    for(i=0; i<N; i++)
    {
       //The particles lie on the x-y plane at the beginning
       double comx = distrx(rngx);
       double comy = distry(rngy);
       double comz = 0.0;
  
       position[0][i] = comx;
       position[1][i] = comy;
       position[2][i] = comz;
    }



/*********************************/
/*Write the initial configuration*/
/*********************************/
string name1("initial_configuration_dipole_N");
string name2("_d");
string name3("_q");

stringstream writeFile;
writeFile << name1 << N << name2 << d << name3 << q;

ofstream write(writeFile.str().c_str());
cout << "writing on .... " << writeFile.str().c_str() <<endl;

//set precision and the number of decimals to be printed always
write.precision(9);
write.setf(ios::fixed);
write.setf(ios::showpoint);


  write << "LAMMPS data file initial configuration dipole; timestep = 0" << endl;

  write << N << " atoms"     << endl;
  write << "\n";

  write << 1 << " atom types"     << endl;
  write << "\n";

  write << -L/2.0 << " " << L/2.0   << " xlo xhi" << endl;
  write << -L/2.0 << " " << L/2.0   << " ylo yhi" << endl;
  write << -5.0   << " " << 5.0     << " zlo zhi" << endl;
  write << "\n";


  //Since there is only one type of particle we set only one mass
  write << "Masses\n" << endl;
  for(i=0; i<1;i++)
  {
      write << i+1 << " 1" << endl;
  }
  write << "\n";





  //atom-ID; atom-type; x y z; diameter; density; charge; dipole-moment

  /*The diameter (d) in simulation units: 1 sigma= 1 mm*/
  /*Atom density (1.0) (mass/sigma^3)*/
  /*Atom charge(1.0): q* = q / (4 pi perm0 sigma epsilon)^1/2*/
  /*COmpoents of dipole moment(0.0): *mu = mu / (4 pi perm0 sigma^3 epsilon)^1/2*/
  //Note that the dipole moment at the beginning is either along the x or z directions only

  write << "Atoms\n" << endl;

  for(i=0; i<N; i++ )
  {
      write << i+1 << " " << 1 << " " << position[0][i] << " " << position[1][i] << " " << position[2][i] << " " << d << " " << d << " " << q << " 0.0 0.0 1.0" <<endl;
  }
  write << "\n";


  // atomid; vx vy vz; wx wy wz
  write << "Velocities\n" << endl;
  for(i=0; i<N; i++ )
  {
      write << i+1 << " 0 0 0 0 0 0" << endl;
  } 
  write << "\n";

return 0; 
}

