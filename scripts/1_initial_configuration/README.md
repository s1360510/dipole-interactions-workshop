This program creates the initial configuration of particles with magnetic dipole moment oriented alog the z direction. 

If the size of the box is L in the x and y directions, the particles lie on the xy plane, filling the space in the range (-Lx/4,Lx/4) and (-Ly/4, Ly4).

In order to run the programm c++11 must be installed in your computer. If this is the case you have to open a terminal and type:

     c++ -std=c++11 setup_for_lammps.c++ -o setup_for_lammps.out -lm
     
Then run the programm by typing

    ./setup_for_lammps.out

The programm will ask the user to introduce:

    a) Number of particles in the system: 1000
    b) Diameter of particles (in simulation units): 1.0
    c) Charge of particles (in simulation units): 1.0  
    d) Box size along the x-y directions (in simulation units): 72 

This will create the file: **initial_configuration_dipole_N1000_d1_q1**. Copy this file into the folder **2_equilibration_with_Soft**
