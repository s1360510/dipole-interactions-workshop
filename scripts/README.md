Here you will find all the ingredients to run a LAMMPS simulation for the system decribed before.

This requires that we have three important files:

**1.-** An initial configuration: with all the attributes that the particles have written with a specific syntax (check the folder **1_initial_configuration**)

**2.-** A LAMMPS script containig all the information related with the conditions of the system (temperature, viscosity, ..., etc) and the way that equations of motion will be integrated (langevin, nve, nvt, ..., etc). Please see inside **2_equilibration_with_Soft**

**3.-**It is assumed that you have compiled a LAMMPS executable (**lmp_serial**) with the custom potentials to account for the dipole-dipole interaction. If this is not the case, please check the files inside the folder **lammps_compilation**.