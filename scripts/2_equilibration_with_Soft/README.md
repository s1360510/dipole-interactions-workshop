The lammps script **commands_lammps_dipole_equisoft** contains the command lines to mimic a system with a soft interaction (see [LINK](https://lammps.sandia.gov/doc/pair_soft.html)) between particles.

This is done because the position of the particles in the initial configuration was generated randomly. Therefore, some of them could be overlapped. If a LJ potential is used, this could cause the simulation to crash. In order to avoid overlapping therefore we use the soft potential.

Please note that the dipole and the Lennard Jhones interactions are turnoff at this stage of the simmulation.

In order to run the lammps script we need to copy the executable we created before (lmp_serial) inside this folder. Then we open a terminal in this location and type:

     ./lmp_serial -in commands_lammps_dipole_equisoft

###Note:
After running the simulation copy the restar file **dipole_soft.N1000.Restart.500000** (that is inside the folder **dipole_N1000**) into the folder **3_changing_dielectric/dielectric_0.10/1_high_temperature/**

In this example we ran the simulation for only 500 000 timesteps, but for a real equilibration we will require the simulation to run for longer (~10⁷ , 10⁹ timesteps or atleast until the radius of gyration becomes constant)


