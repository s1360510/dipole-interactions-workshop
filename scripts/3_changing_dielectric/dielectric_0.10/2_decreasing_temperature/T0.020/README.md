Before running the script inside this folder we assume that:

1.- The reader ran an equilibration using a soft potential.

2.- He/She took the restart file after the previous equilibration. With this configuration the reader ran and equilibration at high temperature replacing the soft potential by our custom potential.

Then we will copy here the restart file (**dipole.N1000.die0.10.Restart.50000**) created after this second run, and located into **3_changing_dielectric/dielectric_0.10/1_high_temperature/dipole_N1000/**.

In order to run the sript we type:

      ./lmp_serial -in commands_lammps_dipole

The only difference with the previous run is that this time we decrease the temperature of the system.

