The lammps script **commands_lammps_dipole_equiLJ** contains the command lines to mimic a system with our custom potentials **pair_style lj/cut/mcoul/cut**, after the equilibration with the soft potential has taken place. In addition we use the **dielectric 0.10** command. The value of this variable is related to the magnitude of the dipoledipole interaction: the smaller the dielectric, the larger the repulsive interaction between particles.

In order to run the lammps script we need to copy the executable we created before (**lmp_serial**) inside this folder and also the restart file **dipole_soft.N1000.Restart.500000** that is inside **2_equilibration_with_Soft/dipole_N1000/**. Then we open a terminal in this location and type:

     ./lmp_serial -in commands_lammps_dipole_equiLJ
     
###Note:
Copy the restart file **dipole.N1000.die0.10.Restart.50000** (located inside **dipole_N1000/**) into the folder **3_changing_dielectric/dielectric_0.10/2_decreasing_temperature/T0.020**.

In this example we ran the simulation for only 500 000 timesteps, but for a real equilibration we will require the simulation to run for longer (~5x10⁷  timesteps this is until the MSD becomes constant)


